(include-book "arithmetic/top-with-meta" :dir :system)

(defun fibonacci-iter (n a b)
  (declare (xargs :guard (and (natp n)
                              (natp a)
                              (posp b))))
  (cond ((zp n) a)
        (t (fibonacci-iter (- n 1) b (+ a b)))))

(defun fibonacci* (n a b)
  (declare (xargs :guard (and (natp n)
                              (natp a)
                              (posp b))))
  (cond
   ((zp n) a)
   ((= n 1) b)
   (t (+ (fibonacci* (- n 1) a b)
         (fibonacci* (- n 2) a b)))))

(defthm fibonacci-iter-equal-fibonacci*-lemma-1
  (implies (and (natp a)
                (posp b))
           (equal (fibonacci-iter 1 a b) b))
  :hints (("Goal" :expand ((fibonacci-iter 1 a b)))))

(defthm fibonacci-iter-equal-fibonacci*
  (implies (and (natp n)
                (natp a)
                (posp b))
           (equal (fibonacci-iter n a b)
                  (fibonacci* n a b))))

(defun fibonacci (n)
  (cond ((zp n) 0)
        ((= n 1) 1)
        (t (+ (fibonacci (- n 1))
              (fibonacci (- n 2))))))

(defthm fibonacci*-equal-fibonacci
  (equal (fibonacci* n 0 1) (fibonacci n)))

(defthm fibonacci-iter-equal-fibonacci
  (implies (natp n)
           (equal (fibonacci-iter n 0 1)
                  (fibonacci n))))
