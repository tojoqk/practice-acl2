(defun sum (xs)
  (if (endp xs)
      0
      (+ (car xs) (sum (cdr xs)))))

(defun exists-n>=2 (xs)
  (if (endp xs)
      nil
      (or (>= (car xs) 2)
          (exists-n>=2 (cdr xs)))))

(defthm pigeonhole
  (implies (and (nat-listp xs)
                (< (length xs) (sum xs)))
           (exists-n>=2 xs)))
