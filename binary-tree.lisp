(defun nodep (x)
  (declare (xargs :guard t))
  (and (consp x)
       (consp (car x))
       (consp (cdr x))
       (consp (cddr x))
       (null (cdddr x))))

(defun emptyp (x)
  (declare (xargs :guard t))
  (not (nodep x)))

(defun keyp (x)
  (declare (xargs :guard t))
  (rationalp x))

(defun node-key+value (x)
  (declare (xargs :guard (nodep x)))
  (car x))

(defun node-key (x)
  (declare (xargs :guard (nodep x)))
  (car (node-key+value x)))

(defun node-value (x)
  (declare (xargs :guard (nodep x)))
  (cdr (node-key+value x)))

(defun node-left (x)
  (declare (xargs :guard (nodep x)))
  (cadr x))

(defun node-right (x)
  (declare (xargs :guard (nodep x)))
  (caddr x))

(defun binary-tree-every-key< (x k)
  (declare (xargs :guard (keyp k)))
  (if (emptyp x)
      t
      (and (keyp (caar x))
           (< (caar x) k)
           (binary-tree-every-key< (node-left x) k)
           (binary-tree-every-key< (node-right x) k))))

(defun binary-tree-every-key> (x k)
  (declare (xargs :guard (keyp k)))
  (if (emptyp x)
      t
      (and (keyp (caar x))
           (> (caar x) k)
           (binary-tree-every-key> (node-left x) k)
           (binary-tree-every-key> (node-right x) k))))

(defun binary-treep (x)
  (declare (xargs :guard t))
  (if (emptyp x)
      (null x)
      (and (keyp (node-key x))
           (let ((l (node-left x))
                 (r (node-right x)))
             (and (binary-treep l)
                  (binary-treep r)
                  (binary-tree-every-key< l
                                          (node-key x))
                  (binary-tree-every-key> r
                                          (node-key x)))))))

(defun binary-tree-insert (x k v)
  (declare (xargs :guard (and (binary-treep x) (keyp k))))
  (cond ((emptyp x) (list (cons k v) nil nil))
        ((= k (node-key x))
         (list (cons k v) nil nil))
        ((< k (node-key x))
         (list (node-key+value x)
               (binary-tree-insert (node-left x) k v)
               (node-right x)))
        (t
         (list (node-key+value x)
               (node-left x)
               (binary-tree-insert (node-right x) k v)))))

(defthm keyp/node-key/binary-tree-insert
  (implies (and (keyp k)
                (binary-treep x))
           (rationalp (car (car (binary-tree-insert x k v))))))

(defthm right-inserted-binary-tree-key-is-greater-lemma-1
  (implies (and (keyp k)
                (binary-treep x)
                (nodep x)
                (> k (node-key x)))
           (equal (binary-tree-every-key> (binary-tree-insert (caddr x) k v)
                                          (car (car x)))
                  (binary-tree-every-key> (caddr x)
                                          (car (car x)))))
  :hints (("Goal" :expand ((binary-tree-insert (caddr x) k v)))))

(defthm right-inserted-binary-tree-key-is-greater
  (implies (and (keyp k)
                (binary-treep x)
                (nodep x)
                (> k (node-key x)))
           (binary-tree-every-key> (binary-tree-insert (caddr x) k v)
                                   (car (car x)))))

(defthm left-inserted-binary-tree-key-is-less-lemma-1
  (implies (and (keyp k)
                (binary-treep x)
                (nodep x)
                (< k (node-key x)))
           (equal (binary-tree-every-key< (binary-tree-insert (cadr x) k v)
                                          (car (car x)))
                  (binary-tree-every-key< (cadr x)
                                          (car (car x)))))
  :hints (("Goal" :expand ((binary-tree-insert (cadr x) k v)))))

(defthm left-inserted-binary-tree-key-is-less
  (implies (and (keyp k)
                (nodep x)
                (binary-treep x)
                (< k (node-key x)))
           (binary-tree-every-key< (binary-tree-insert (cadr x) k v)
                                   (car (car x)))))

(defthm inserted-binary-tree-is-binary-tree
  (implies (and (keyp k)
                (binary-treep x))
           (binary-treep (binary-tree-insert x k v))))

(defun every-car-keyp (x)
  (declare (xargs :guard t))
  (if (atom x)
      (null x)
      (and (consp (car x))
           (keyp (caar x))
           (every-car-keyp (cdr x)))))

(defun alist-to-binary-tree (alist)
  (declare (xargs :guard (and (alistp alist)
                              (every-car-keyp alist))))
  (if (endp alist)
      nil
      (binary-tree-insert
       (alist-to-binary-tree (cdr alist))
       (caar alist)
       (cdar alist))))

(defun binary-tree-to-alist (x)
  (declare (xargs :guard (binary-treep x)))
  (if (emptyp x)
      nil
      (append (append (binary-tree-to-alist (node-left x))
                      (list (cons (node-key x) (node-value x))))
              (binary-tree-to-alist (node-right x)))))

(defun binary-tree-search (x k)
  (declare (xargs :guard (and (binary-treep x) (keyp k))))
  (cond ((emptyp x) nil)
        ((= k (node-key x)) (node-key+value x))
        ((< k (node-key x)) (binary-tree-search (node-left x) k))
        (t (binary-tree-search (node-right x) k))))

(defthm assoc-append
  (implies (and (alistp x)
                (alistp y))
           (equal (assoc k (append x y))
                  (or (assoc k x)
                      (assoc k y)))))

(defthm assoc-append-3
  (implies (and (alistp x)
                (alistp y)
                (alistp z))
           (equal (assoc k (append (append x y) z))
                  (or (assoc k x)
                      (assoc k y)
                      (assoc k z)))))

(defthm alistp/binary-tree-to-alist
  (implies (binary-treep x)
           (alistp (binary-tree-to-alist x))))

(defthm binary-tree-search-is-assoc-lemma-1
  (implies (and (binary-treep x)
                (keyp k1)
                (keyp k2)
                (<= k2 k1)
                (binary-tree-every-key< x k2))
           (not (assoc-equal k1 (binary-tree-to-alist x)))))

(defthm binary-tree-search-is-assoc-lemma-2
  (implies (and (binary-treep x)
                (keyp k)
                (binary-tree-every-key< x k))
           (not (assoc k (binary-tree-to-alist x)))))

(defthm binary-tree-search-is-assoc
  (implies (and (binary-treep x)
                (keyp k))
           (equal (binary-tree-search x k)
                  (assoc k (binary-tree-to-alist x))))
  :rule-classes nil
  :hints (("Subgoal *1/6'4'"
           :use ((:instance binary-tree-search-is-assoc-lemma-1
                            (k1 k)
                            (k2 (car (car x)))
                            (x (cadr x))))
           :in-theory (disable binary-tree-search-is-assoc-lemma-1))))

(defthm binary-tree-search-binary-tree-insert
  (equal (binary-tree-search (binary-tree-insert x k v) k)
         (cons k v)))
