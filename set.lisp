(defun emptyp (x)
  (declare (xargs :guard t))
  (atom x))

(defun setp (x)
  (declare (xargs :guard t))
  (if (emptyp x)
      (null x)
      (and (true-listp x)
           (if (member-equal (car x) (cdr x))
               nil
               (setp (cdr x))))))

(defun elementp (e x)
  (declare (xargs :guard (setp x)))
  (member-equal e x))

(defun cardinal (x)
  (declare (xargs :guard (setp x)))
  (if (emptyp x)
      0
      (+ 1 (cardinal (cdr x)))))

(defthm setp-union-equal-lemma-1
  (implies (and (setp x)
                (setp y)
                (not (member (car x) y)))
           (not (member (car x) (union-equal (cdr x) y)))))

(defthm setp-union-equal
  (implies (and (setp x)
                (setp y))
           (setp (union-equal x y))))

(defthm setp-intersection-equal-lemma-1
  (implies (and (setp x)
                (setp y)
                (member-equal (car x) y))
           (not (member (car x) (intersection-equal (cdr x) y)))))

(defthm setp-intersection-equal
  (implies (and (setp x)
                (setp y))
           (setp (intersection-equal x y))))

(defthm subsetp-cdr
  (implies (subsetp x (cdr y))
           (subsetp x y)))

(defthm subsetp-reflexivity
  (subsetp x x))

(defun cartesian-product-1 (e x)
  (if (emptyp x)
      nil
      (cons (cons e (car x))
            (cartesian-product-1 e (cdr x)))))

(defun cartesian-product (x y)
  (if (emptyp x)
      nil
      (union-equal (cartesian-product-1 (car x) y)
                   (cartesian-product (cdr x) y))))

(defthm member-cartesian-product-1
  (iff (member-equal (cons e x)
                     (cartesian-product-1 e y))
       (member-equal x y)))

(defthm setp/cartesian-product-1
  (implies (setp x)
           (setp (cartesian-product-1 e x))))

(defthm setp/cartesian-product
  (implies (and (setp x)
                (setp y))
           (setp (cartesian-product x y))))

(defthm theorem-of-cartesian-product
  (implies (and (setp x)
                (setp y)
                (member a x)
                (member b y))
           (consp (member (cons a b)
                          (cartesian-product x y)))))
